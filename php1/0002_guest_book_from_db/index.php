<?php

require_once 'connect.php';
require_once 'functions.php';


if (!empty($_POST) && !isset($_POST['submit'])) {
    save_mess($db);
    header("Location: {$_SERVER['PHP_SELF']}");
    die;
}

$res = mysqli_query($db, "SELECT id, name, text, date  FROM guest_book ORDER BY id DESC");
$data = mysqli_fetch_all($res, MYSQLI_ASSOC);

?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Гостевая книга из БД</title>
</head>
<body>

<form action="index.php" method="post">
    <p class="">
        <label for="name">Имя:</label><br>
        <input type="text" name="name" placeholder="Введите имя" id="name" style="padding: 10px;">
    </p>
    <p class="">
        <label for="text">Текст:</label><br>
        <textarea name="text" id="text" placeholder="И текст" style="padding: 10px;" cols="80" rows="5"></textarea>
    </p>
    <button type="submit" style="padding: 10px">Написать</button>
</form>

<hr>
<?php

if (!empty($_POST) && isset($_POST['submit'])) {
    $id = (int)$_POST['submit'];
    mysqli_query($db, "DELETE FROM guest_book WHERE id = {$id}");
    header("Location: {$_SERVER['PHP_SELF']}");
    die;
}

foreach ($data as $item) { ?>
    <div class="messages" style="padding: 10px; margin-bottom: 10px; border: 1px solid black;">
        <p style="margin-top: 0; font-size: 12px; font-style: italic; text-decoration: underline;">Автор: <b
                    style="font-size: 14px;"><?php echo $item['name'] ?></b> | Дата: <?php echo $item['date'] ?> </p>
        <div style="font-size: 20px;"><?php echo $item['text']; ?></div>
        <form action="index.php" method="post">
            <button type="submit" style="padding: 10px; margin-top: 10px" name="submit"
                    value="<?php echo $item['id'] ?>">Удалить
            </button>
        </form>
    </div>
<?php }

?>
</body>
</html>