-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Окт 02 2019 г., 15:10
-- Версия сервера: 8.0.17
-- Версия PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `guest_book`
--

-- --------------------------------------------------------

--
-- Структура таблицы `guest_book`
--

CREATE TABLE `guest_book` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `text` text NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `guest_book`
--

INSERT INTO `guest_book` (`id`, `name`, `text`, `date`) VALUES
(1, 'Zarathustra', 'Поистине, человек — это грязный поток!', '2019-10-01 14:05:52'),
(2, 'Zarathustra', 'С человеком происходит то же, что и с деревом. Чем больше стремится он вверх, к свету, тем глубже впиваются корни его в землю, вниз, в мрак и глубину, ко злу.', '2019-10-01 14:05:52'),
(3, 'Zarathustra', 'Надо научиться любить себя самого. Так учу я. Любовью цельной и здоровой: чтобы сносить себя самого и не скитаться всюду.', '2019-10-01 14:06:31'),
(4, 'Zarathustra', 'Жизнь есть родник радости, но всюду, где пьёт отребье, все родники бывают отравлены.', '2019-10-01 14:06:31'),
(5, 'Zarathustra', 'Если есть враг у вас, не платите ему за зло добром: ибо это пристыдило бы его. Напротив, докажите ему, что он сделал для вас нечто доброе.', '2019-10-02 09:03:44'),
(6, 'Zarathustra', 'Тому повелевают, кто не может повиноваться самому себе.', '2019-10-02 10:53:47');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `guest_book`
--
ALTER TABLE `guest_book`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `guest_book`
--
ALTER TABLE `guest_book`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
