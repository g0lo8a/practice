<?php

function save_mess()
{
    $str = $_POST['name'] . '|' . $_POST['text'] . '|' . date('d M Yг. H:i') . "\n***\n";
    file_put_contents('guestBook.txt', $str, FILE_APPEND);
}

function get_mess()
{
    return file_get_contents('guestBook.txt');
}

function array_mess($messages)
{
    $messages = explode("\n***\n", $messages);
    array_pop($messages);
    return array_reverse($messages);
}

function get_format_message($arr)
{
    return explode('|', $arr);
}