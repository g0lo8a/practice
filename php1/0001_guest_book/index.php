<?php

require_once 'functions.php';

if (!empty($_POST)) {
    save_mess();
    header("Location: {$_SERVER['PHP_SELF']}");
    die;
}

$messages = get_mess();
$messages = array_mess($messages);

?>

<!doctype html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Гостевая книга</title>
</head>
<body>

<form action="index.php" method="post">
    <p class="">
        <label for="name">Имя:</label><br>
        <input type="text" name="name" placeholder="Введите имя" id="name" style="padding: 10px">
    </p>
    <p class="">
        <label for="text">Текст:</label><br>
        <textarea name="text" id="text" placeholder="И текст" style="padding: 10px"></textarea>
    </p>
    <button type="submit" style="padding: 10px">Написать</button>
</form>

<hr>

<?php
if (!empty($messages)) {
    foreach ($messages as $message) { ?>
        <?php $message = get_format_message($message); ?>
        <div class="messages" style="padding: 10px; margin-bottom: 10px; border: 1px solid black;">
            <p style="font-size: 12px; font-style: italic; text-decoration: underline;">Автор: <b style="font-size: 14px;"><?php echo $message[0] ?></b> | Дата: <?php echo $message[2] ?> </p>
            <div style="font-size: 20px;"><?php echo nl2br(htmlspecialchars($message[1])); ?></div>
        </div>
    <?php }
}
echo md5('g0lo8a');
?>
</body>
</html>